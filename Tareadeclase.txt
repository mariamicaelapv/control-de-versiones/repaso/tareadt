 Declaración e inicialización de un array de enteros de tamaño 10
1 Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición
2 Modificación del array y visualización del mismo: en las posiciones pares del array se modificará el contenido que tenga por 0
3 Visualización del número de ceros que hay en el array.
4 Visualización del número de elementos distintos de cero que hay en el array.